---
layout: default
title: 'Basis: A translator from capitalism to socialism'
network_animation: true
leader:
  image: "/assets/images/network-home.jpg"
  text: "A translator from capitalism to socialism"
  subtext: "The Basis project enables a decentralized &amp; profitless economic network within the confines of capitalism"
  button:
    text: 'Get involved &raquo;'
    url: '/contribute'
---

<div class="home-opener uk-section uk-section">
	<div class="uk-container uk-container-small">
		<p class="uk-text-lead">
			Stop waiting for revolution or for stuffy politicians to enact change while capitalism charges
			us rent to fund the ravaging of our planet. Take action and help build what's next.
		</p>
		<p>
			Basis is an <a href="https://gitlab.com/basisproject/">open source</a> set of tools that enables
			a decentralized network of co-ops that eats capitalism over time, converting private resources
			to shared and supporting production based on use and need instead of profit. Using the
			<a href="https://en.wikipedia.org/wiki/Free_association_(Marxism_and_anarchism)">free association of producers</a>
			and <a href="https://en.wikipedia.org/wiki/Calculation_in_kind">in-kind cost tracking</a> as core
			concepts, the project forges an ecologically-focused transparent economy where members are free
			to produce their own wants and needs without being subject to forces they have no say in.
		</p>

		<div class="uk-grid-large uk-grid-divider@s uk-padding-small uk-padding-remove-bottom uk-text-center" uk-grid>
			<div class="uk-width-1-2@s uk-text-right@s">
				<a class="uk-button uk-button-default" href="{{ "/contribute" | prepend: site.baseurl }}">Get involved &raquo;</a>
			</div>
			<div class="uk-width-1-2@s uk-text-left@s">
				<a class="uk-button uk-button-default" href="https://www.reddit.com/r/basisproject">Join the community&nbsp;&nbsp;<icon>&#xF281;</icon></a>
			</div>
		</div>
	</div>
</div>

<div class="home-grid uk-section uk-section-muted">
	<div class="uk-container uk-container-small">
		<h2 class="uk-text-center">What is Basis?</h2>
		<div class="uk-grid-match uk-text-center" uk-grid>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<div class="uk-card uk-card-default uk-card-body">
					<icon>&#xe80a;</icon>
					<h3 class="uk-text-lead uk-margin-remove">Shared assets</h3>
					<p class="uk-margin-remove">
						Tools for managing the use of communally-owned assets (housing, factories, etc)
					</p>
				</div>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<div class="uk-card uk-card-default uk-card-body">
					<icon>&#xF19C;</icon>
					<h3 class="uk-text-lead uk-margin-remove">Banking</h3>
					<p class="uk-margin-remove">
						Facilitates and automates interfacing with the outside market system
					</p>
				</div>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<div class="uk-card uk-card-default uk-card-body">
					<icon>&#xE808;</icon>
					<h3 class="uk-text-lead uk-margin-remove">Cost tracking</h3>
					<p class="uk-margin-remove">
						Determines costs of labor and resources (for example fossil fuels) for each product and service
					</p>
				</div>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<div class="uk-card uk-card-default uk-card-body">
					<icon>&#xE809;</icon>
					<h3 class="uk-text-lead uk-margin-remove">Governance</h3>
					<p class="uk-margin-remove">
						Supports self-management of companies, regions, and ecological resources
					</p>
				</div>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<div class="uk-card uk-card-default uk-card-body">
					<icon>&#xF291;</icon>
					<h3 class="uk-text-lead uk-margin-remove">Public market</h3>
					<p class="uk-margin-remove">
						A place where members can discover and order products and services from each other (like a big socialist Amazon)
					</p>
				</div>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<div class="uk-card uk-card-default uk-card-body">
					<icon>&#xE802;</icon>
					<h3 class="uk-text-lead uk-margin-remove">Public companies</h3>
					<p class="uk-margin-remove">
						Allows defining and transparently operating social projects (hospitals, pharma research, schools, etc)
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="uk-section uk-section-small uk-section-muted">
	<div class="uk-container uk-container-small">
		<h2>
			Project news
			<small class="uk-text-small"><a href="{{ "/posts" | prepend: site.baseurl }}">All news &raquo;</a></small>
		</h2>
		{% include post-summary.html %}
	</div>
</div>

<div class="uk-section uk-section">
	<div class="uk-container uk-container-small uk-text-center">
		<a href="{{ "/contribute" | prepend: site.baseurl }}" class="button-contribute uk-button uk-button-primary uk-button-large">Want to contribute?</a>
		<p>Come join us at the <a href="https://www.reddit.com/r/basisproject">Basis community</a> or
		<a href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=type%3Adiscussion">join in on ongoing discussions!</a></p>
	</div>
</div>

<div class="uk-section uk-section uk-section-muted">
	<div class="uk-container uk-container-small">
		<h1>Why build this?</h1>
		<p>As capitalism has given to us, it has also taken away. It gives us a wage but takes our sense of ownership and purpose. It gives us houses but forces us into debt to live in them. It gives us hospitals but bankrupts us when we use them. It hands our political self-determination to banks and corporations and placates us with an endless stream of consumer trinkets, all while telling us we have "choice."</p>
		<p>The goal of Basis is to enable a system where we keep our sense of ownership and purpose, we keep the houses and hospitals we have built, we keep our self-determination, but shed the perversion of capital and the alienation it brings with it. </p>
		<p>Imagine a world where instead of asking "Will this make money?" people ask "Will this be useful to others?" It's a small shift in thinking, but would profoundly change our relationships to production, the environment, and each other. What would the world look like if we felt a connection to not just the things we made, but with the meaning behind our work? Basis was built with the goal of enabling an economy where profit is removed from production, replacing the random coldness of markets with a sense of connected purpose.</p>
	</div>
</div>
