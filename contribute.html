---
layout: default
title: 'Contribute to the Basis project'
body_class: 'contributing'
---

<div class="uk-section uk-section">
	<div class="uk-container uk-container-xsmall">
		<h1>Contribute to Basis: We'd love your help!</h1>
		<p>
			This project is built off of the ideas and voices of the people who are eventually going
			to be using it to break out of the capitalist system. If you have thoughts, input, or
			just want to get to know the project better, check out the discussions below!
		</p>
		<div class="uk-grid-small uk-grid-divider@s uk-padding-small uk-padding-remove-bottom uk-text-center" uk-grid>
			<div class="uk-width-1-2@s">
				<a class="uk-button uk-button-default" href="#talk-theory">Want to talk theory?</a>
			</div>
			<div class="uk-width-1-2@s">
				<a class="uk-button uk-button-default" href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=project%3Acore">Help us code &raquo;</a>
			</div>
		</div>
	</div>
</div>
<div id="talk-theory" class="uk-section uk-section-small uk-section-muted">
	<div class="uk-container uk-container-small">
		<h2 class="uk-text-center">Ongoing discussions</h2>
		<div class="uk-grid-match uk-grid-medium uk-margin-medium-bottom icon-grid" uk-grid>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<a href="#banking" class="uk-card uk-card-default uk-card-body">
					<div class="uk-card-badge uk-label"><icon>&#xF19C;</icon></div>
					<h3 class="uk-card-title">Banking</h3>
					<p>
						How does decentralized banking translate between the network and capitalist markets?
					</p>
				</a>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<a href="#property-rights" class="uk-card uk-card-default uk-card-body">
					<div class="uk-card-badge uk-label"><icon>&#xe80a;</icon></div>
					<h3 class="uk-card-title">Property rights</h3>
					<p>
						How is usage of shared resources mediated without markets?
					</p>
				</a>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<a href="#governance" class="uk-card uk-card-default uk-card-body">
					<div class="uk-card-badge uk-label"><icon>&#xE809;</icon></div>
					<h3 class="uk-card-title">Governance</h3>
					<p>
						What are the technical implications of self-determination?
					</p>
				</a>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<a href="#privacy" class="uk-card uk-card-default uk-card-body">
					<div class="uk-card-badge uk-label"><icon>&#xF21B;</icon></div>
					<h3 class="uk-card-title">Privacy</h3>
					<p>
						How do members of the network protect personal information such as purchases?
					</p>
				</a>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<a href="#public-market" class="uk-card uk-card-default uk-card-body">
					<div class="uk-card-badge uk-label"><icon>&#xF291;</icon></div>
					<h3 class="uk-card-title">Public market</h3>
					<p>
						How do network producers and consumers find each other?
					</p>
				</a>
			</div>
			<div class="uk-width-1-2@s uk-width-1-3@m">
				<a href="#economics" class="uk-card uk-card-default uk-card-body">
					<div class="uk-card-badge uk-label"><icon>&#xE800;</icon></div>
					<h3 class="uk-card-title">Economics</h3>
					<p>
						How is distributed production organized without money and prices?
					</p>
				</a>
			</div>
		</div>
		<p class="uk-text-small uk-text-center">
			Don't forget, you can <a href="https://www.reddit.com/r/basisproject/">join us on Reddit <icon>&#xF281;</icon></a> for more discussion.
		</p>
	</div>
</div>

<div class="uk-section uk-section-small">
	<div class="uk-container uk-container-small discussions">
		<h2 id="banking">
			Banking
		</h2>
		<p>
			Banking is arguably the most important part of the project. It's the entire translation
			layer between profitless production and capitalist markets. It creates a protective
			membrane around the network and allows it to grow just like a private company would.
			Banking has the following goals:
		</p>
		<ul>
			<li>Facilitate seamless transactions between in-network companies and capitalist entities</li>
			<li>Allow network members to exchange their credits for local currency</li>
			<li>Maintain a decentralized infrastructure to avoid single points of failure</li>
		</ul>
		<p>
		Although many of the interactions of banking <a href="{{ '/paper#chapter-6-banking' | prepend: site.baseurl }}">have been described in the paper,</a>
			much of it remains to be determined.
		</p>
		<p>
			Join on some of the <a href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=tag%3Abanking">banking discussions.</a>
		</p>

		<h2 id="property-rights">
			Property rights
		</h2>
		<p>
			One of the primary goals of the Basis project is the conversion of privately-owned
			productive and housing resources to be commonly held. However, the story doesn't
			end there. We need ways to determine who ultimately manages which shared resources,
			and then how use of these resources is determined. Who stays in which house? What 
			company uses which factory? Who can log or hike or farm in the forest?
		</p>
		<p>
			What ultimately makes sense is that the people most impacted by a resource have the
			most say in managing it. How do we measure this impact, and how do we translate it into
			a well-defined process? And how do we enable people to build on this process without
			artificial limitations?
		</p>
		<p>Help us in <a href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=tag%3Aproperty">discussions regarding property.</a>

		<h2 id="governance">
			Governance
		</h2>
		<p>
			What is the good of common ownership of anything if you have no say in how it's managed?
			Governance is about harnessing democracy and consensus to give people a say in the things
			that affect them. Not only this, but the ultimate goal of governance is to allow people to
			propose changes to the system itself and collectively decide whether it should be implemented
			or not.
		</p>
		<p>
			This topic is very much about voting, security, and different methods of democracy. If this
			interests you, <a href="https://gitlab.com/basisproject/tracker/-/issues?label_name[]=tag%3Agovernance">discuss governance with us.</a>
		</p>

		<h2 id="privacy">
			Privacy
		</h2>
		<p>
			Cash has this wonderful property of being anonymous. No government agency can see the things
			you buy. No corporation can connect your purchases to your ad profile. Credit cards
			and other electronic payment methods erode this attribute of cash, and while the general
			public seems fine with the trade-off, the aggregation of this kind of data can ultimately
			prove very harmful.
		</p>
		<p>
			So while Basis makes <em>all productive transactions</em> open to member scrutiny, it also
			protects the purchases of individual members such that only they and the company they're
			buying from know all the details of the transaction. To the system, and thus the prying
			eyes of of the public, we can see that <em>a</em> transaction happened. Never <em>your</em>
			transaction happened. <!-- I don't own a... --> The buyer is completely obscured.
		</p>
		<p>
			Beyond buying things, another essential facet to Basis is voting. While some groups might
			want open voting, others might require anonymity and privacy in their democratic processes.
			This needs to be supported fully.
		</p>
		<p>
			If you have experience in cryptography, data privacy, or electronic voting, we'd 
			<a href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=tag%3Aprivacy">love to get your input</a>.
		</p>

		<h2 id="public-market">
			Public market
		</h2>
		<p>
			A large part of this project is cost tracking. Cost tracking requires that people buy things
			through the system itself. Think of it like a commonly-owned Amazon.com except that the
			members of the system decide where the profits go.
		</p>
		<p>
			Check out <a href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=project%3Amarket">public market discussions</a>.
		</p>

		<h2 id="economics">
			Economics
		</h2>
		<p>
			While the economics of Basis has received a lot of thought and research and is currently
			<a href="https://gitlab.com/basisproject/core">being implemented in code</a>, some aspects
			are still being determined. For instance, members can define public companies with subsidized
			costs. Where do these subsidies come from? In Basis, there's no concept of costs disappearing.
			All costs must be accounted for.
		</p>
		<p>
			Do we implement taxation? Or are there other methods of covering costs?
			<a href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=tag%3Aeconomics">Help us figure these things out.</a>
		</p>
		<p>
			Also, what automated controls do we place on the economy to guide things in a direction
			favorable to all participants? You might be interested in discussing
			<a href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=tag%3Acybernetics">cybernetics.</a>
		</p>

		<h2 id="your-discussions">
			All discussions
		</h2>
		<p>
			See the <a href="https://gitlab.com/basisproject/tracker/-/issues?label_name%5B%5D=type%3Adiscussion">list of all ongoing project discussions</a>
			to find one that interests you.
		</p>
		<p>
			Want to bring up a topic that isn't here? Have a concern or an idea for the project? Stop by the
			<a href="https://gitlab.com/basisproject/tracker/-/issues">project tracker</a> and open a new
			issue or join us at the <a href="https://www.reddit.com/r/basisproject/">community forum!</a>
		</p>
	</div>
</div>
